extends "res://ui/titled_container.gd"
class_name LifeSensors


var _sensors := []


func _ready():
	for row in $Crew.get_children():
		for sensor in row.get_children():
			_sensors.append(sensor)


func clear(id: String):
	for sensor in _sensors:
		if sensor.id == id:
			sensor.clear()
			return true


func kill(id: String):
	for sensor in _sensors:
		if sensor.id == id:
			sensor.kill()
			return true


func warn(id: String):
	for sensor in _sensors:
		if sensor.id == id:
			sensor.warn()
			return true

	return false
