extends "res://ui/titled_container.gd"
class_name MessageFeed


signal next_requested

const SCROLL_SPEED := 66.6

var prevent_indicator := false

var _last_message: Message

onready var _container: VBoxContainer = $ScrollContainer/VBoxContainer
onready var _indicator: Label = $Indicator
onready var _message_class = preload("res://ui/messages/message.tscn")


func _ready():
	_indicator.visible = false


func _process(delta: float):
	if not _last_message:
		return

	var last_value = _last_message.percent_visible
	var value = _last_message.percent_visible + delta * SCROLL_SPEED / _last_message.text.length()
	_last_message.percent_visible = min(1, value)

	if last_value < 1.0 and _last_message.percent_visible == 1.0 and not prevent_indicator:
		_indicator.visible = true


func add_message(text: String):
	prevent_indicator = false

	_last_message = _message_class.instance()
	_last_message.owner = _container
	_container.add_child(_last_message)

	_last_message.bbcode_text = text
	_last_message.percent_visible = 0.0


func _on_Clicker_pressed():
	_indicator.visible = false

	if not _last_message:
		return

	if _last_message.percent_visible < 1.0:
		_last_message.percent_visible = 1.0

		if not prevent_indicator:
			_indicator.visible = true
		return

	emit_signal("next_requested")
