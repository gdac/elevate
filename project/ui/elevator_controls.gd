extends "res://ui/titled_container.gd"
class_name ElevatorControls


signal listened(index)
signal selected(index)

var _count := 0
var _listened_once := false

onready var _floors: Array = [
	$VBoxContainer/Controls/Bridge,
	$VBoxContainer/Controls/LivingQuarters,
	$VBoxContainer/Controls/Labs,
	$VBoxContainer/Controls/CargoHold,
	$VBoxContainer/Controls/EngineRoom,
]
onready var _label: Label = $VBoxContainer/Label


func _ready():
	for f in _floors:
		f.connect("listened", self, "_on_ElevatorControl_listened", [f])
		f.connect("selected", self, "_on_ElevatorControl_selected", [f])


func disable_listening():
	_label.text = "Elevator controls disabled"

	for f in _floors:
		f.disable_listening()


func disable_movement():
	for f in _floors:
		f.disable_movement()


func enable_listening(count: int):
	if count == 0:
		_label.text = "You must choose a destination for the elevator"
		return

	if _listened_once:
		_label.text = "You can listen to %s more floors before you must send the elevator" % count
	else:
		_label.text = "You must listen to a floor before you can send the elevator"

	for f in _floors:
		f.enable_listening()


func enable_movement():
	if _listened_once == false:
		return

	for f in _floors:
		f.enable_movement()


func reset():
	for f in _floors:
		f.listened = false


func select(index: int):
	for f in _floors:
		f.selected = f.get_index() == index


func _on_ElevatorControl_listened(control: ElevatorControl):
	var index = control.get_index()
	_floors[index].listened = true
	_listened_once = true
	emit_signal("listened", index)


func _on_ElevatorControl_selected(control: ElevatorControl):
	_listened_once = false
	emit_signal("selected", control.get_index())
