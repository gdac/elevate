extends Control


const CLEAR_COLOUR := Color.lawngreen
const DEATH_COLOUR := Color.red
const WARN_COLOUR := Color.yellow

export(String) var id := "id"
export(String) var full_name := "full_name"
export(String) var occupation := "occupation"

onready var _indicator: ColorRect = $HBoxContainer/CenterContainer/Indicator
onready var _name: Label = $HBoxContainer/Details/Name
onready var _occupation: Label = $HBoxContainer/Details/Occupation


func _ready():
	_name.text = full_name
	_occupation.text = occupation


func clear():
	_indicator.color = CLEAR_COLOUR
	_name.add_color_override("font_color", CLEAR_COLOUR)
	_occupation.add_color_override("font_color", CLEAR_COLOUR)


func kill():
	_indicator.color = DEATH_COLOUR
	_name.add_color_override("font_color", DEATH_COLOUR)
	_occupation.add_color_override("font_color", DEATH_COLOUR)


func warn():
	_indicator.color = WARN_COLOUR
	_name.add_color_override("font_color", WARN_COLOUR)
	_occupation.add_color_override("font_color", WARN_COLOUR)
