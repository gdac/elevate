extends HBoxContainer
class_name ElevatorControl


signal listened
signal selected

export(String) var title := "level_name"

var listened := false
var selected := false

onready var _listen_button: Button = $ListenButton
onready var _send_button: Button = $SendButton


func _ready():
	$ElevatorLevel/Label.text = title
	update()


func _process(delta):
	_send_button.text = "Stay" if selected else "Move"
	$ElevatorLevel.set_colour(Color.yellow if selected else Color.darkcyan)


func disable_listening():
	_listen_button.disabled = true


func disable_movement():
	_send_button.disabled = true


func enable_listening():
	_listen_button.disabled = listened


func enable_movement():
	_send_button.disabled = false


func _on_ListenButton_pressed():
	emit_signal("listened")


func _on_SendButton_pressed():
	emit_signal("selected")
