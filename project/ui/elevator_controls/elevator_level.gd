extends Control


var colour := Color.sienna


func _draw():
	var a := Vector2(0, rect_size.y / 2)
	var b := Vector2(rect_size.y * .5, rect_size.y * .75)
	var c := Vector2(rect_size.x, rect_size.y * .75)
	draw_line(a, b, colour)
	draw_line(b, c, colour)


func set_colour(_colour: Color):
	if _colour == colour:
		return

	colour = _colour
	update()
