extends Control
class_name UI


signal control_listened(index)
signal control_selected(index)
signal next_requested

onready var controls: ElevatorControls = $HBoxContainer/VBoxContainer/ElevatorControls
onready var life_sensors: LifeSensors = $HBoxContainer/VBoxContainer/LifeSensors
onready var message_feed: MessageFeed = $HBoxContainer/MessageFeed
onready var thought: Thought = $Thought


# Called when the node enters the scene tree for the first time.
func _ready():
	controls.disable_listening()
	controls.disable_movement()
	controls.select(2)


func _on_ElevatorControls_listened(index):
	emit_signal("control_listened", index)


func _on_ElevatorControls_selected(index):
	emit_signal("control_selected", index)


func _on_MessageFeed_next_requested():
	emit_signal("next_requested")
