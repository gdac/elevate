tool
extends MarginContainer


export(String) var title := "untitled"

const colour := Color.lawngreen

var _rect_origin := Vector2(16, 16)


func _ready():
	$LabelPivot/Label.text = title


func _draw():
	var _size := rect_size - _rect_origin * 2
	draw_rect(Rect2(_rect_origin, _size), colour, false)
