extends Control
class_name Thought


signal hidden

const FADE_TIME := 0.25

onready var _bg: ColorRect = $Background
onready var _clicker: ToolButton = $Clicker
onready var _message: RichTextLabel = $Background/CenterContainer/Message
onready var _tween: Tween = $Tween


func _ready():
	visible = false


func set_alpha(alpha: float):
	modulate.a = alpha
	visible = alpha > 0
	_bg.rect_scale.y = alpha

	if alpha > _message.percent_visible:
		_message.percent_visible = alpha


func show_thought(thought: String):
	_clicker.visible = false
	_message.bbcode_text = "[center]%s[/center]" % thought
	_message.percent_visible = 0.0
	set_alpha(0)

	_tween.interpolate_method(self, "set_alpha", 0.0, 1.0, FADE_TIME)
	_tween.start()

	yield(_tween, "tween_completed")
	_clicker.visible = true


func _on_Clicker_pressed():
	_clicker.visible = false

	_tween.interpolate_method(self, "set_alpha", 1.0, 0.0, FADE_TIME)
	_tween.start()

	yield(_tween, "tween_completed")
	emit_signal("hidden")
