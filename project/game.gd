extends Node


var _options := {
	"go_passages": [],
	"listen_passages": [],
	"listens_left": 3,
}

onready var _bgm: AudioStreamPlayer = $BGM
onready var _ui: UI = $UI
onready var _yarn_player: YarnPlayer = $YarnPlayer


# Called when the node enters the scene tree for the first time.
func _ready():
	_yarn_player.play("Start")


func _on_UI_control_listened(index):
	_options.listens_left -= 1
	_yarn_player.play(_options.listen_passages[index])


func _on_UI_control_selected(index):
	_options.listens_left = 0
	_ui.controls.reset()
	_ui.controls.select(index)
	_yarn_player.play(_options.go_passages[index])


func _on_UI_next_requested():
	_yarn_player.do_next()


func _on_YarnPlayer_command_triggered(command: String, arguments: Array):
	match command:
		"bgm":
			match arguments[0]:
				"play":
					_bgm.stream = load("res://assets/bgm/%s.ogg" % arguments[1])
					_bgm.play()

				"stop":
					_bgm.stop()

		"clear":
			_ui.life_sensors.clear(arguments[0])

		"kill":
			_ui.life_sensors.kill(arguments[0])

		"set_floor":
			_ui.controls.select(int(arguments[0]))

		"think":
			_ui.thought.show_thought(PoolStringArray(arguments).join(" "))
			yield(_ui.thought, "hidden")

		"warn":
			_ui.life_sensors.warn(arguments[0])

	_yarn_player.do_next()


func _on_YarnPlayer_dialogue_triggered(speaker: String, text: String):
	_ui.message_feed.add_message("[b]%s[/b]: %s" % [speaker, text] if speaker else text)


func _on_YarnPlayer_options_shown(data: Array):
	_options.go_passages = []
	_options.listen_passages = []

	for item in data:
		var a = item.title.split("|")

		if a[0] == "listens":
			_options.listens_left = int(a[1])
			continue

		_options.listen_passages.push_back(a[0])
		_options.go_passages.push_back(a[1])

	_yarn_player.select_option(-1)
	_ui.message_feed.prevent_indicator = true


func _on_YarnPlayer_playback_began():
	_ui.controls.disable_listening()
	_ui.controls.disable_movement()


func _on_YarnPlayer_playback_ended():
	_ui.controls.enable_listening(_options.listens_left)
	_ui.controls.enable_movement()
